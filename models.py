from datetime import datetime
from typing import Optional


class FileProcessResult(object):
    def __init__(
            self,
            file_name: str,
            author: str = None,
            date_time: datetime = None,
            hash_code: str = None
    ):
        if author is None and date_time is None and hash_code is None:
            self.file_name = file_name
            self.is_success = False
        else:
            self.file_name = file_name
            self.is_success = True
            self.author = author
            self.date_time = date_time
            self.hash = hash_code

    file_name: Optional[str]
    is_success: bool
    author: str
    date_time: datetime
    hash: str
