import logging
import sys
from logging.config import DictConfigurator

from processor import Processor

FOLDER_PATH = "C:\\Users\\r.grigoriev\\PycharmProjects\\files_sign_processor\\storage\\files"
BACKUP_FOLDER_PATH = "C:\\Users\\r.grigoriev\\PycharmProjects\\files_sign_processor\\storage\\processed"

SIGN_AUTHOR = "Вержицкий Данил Григорьевич"
SIGN_HASH = "471086fad29a3b30e244c728abc3661ab35c9d50210dcf0e75e03a5b6fdf6436"
SIGN_POSITION = "Директор НФИ КемГУ"

if __name__ == '__main__':
    if FOLDER_PATH == BACKUP_FOLDER_PATH:
        raise NameError

    DictConfigurator(
        {'version': 1,
         'formatters': {
             'normal': {
                 'format': '%(asctime)s %(levelname)-7s =>  %(message)s  [%(filename)s:%(lineno)s]',
             },
         },
         'handlers': {
             'h1': {
                 'class': 'logging.StreamHandler',
                 'stream': sys.stdout,
                 'formatter': 'normal',
                 'level': logging.DEBUG
             },
         },
         'root': {'handlers': ['h1']},
         'disable_existing_loggers': False,
         }
    ).configure()
    logging.getLogger().setLevel(logging.INFO)

    processor = Processor(FOLDER_PATH, BACKUP_FOLDER_PATH, SIGN_AUTHOR, SIGN_HASH, SIGN_POSITION)
    processor.start_process()
