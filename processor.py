import json
import logging
import multiprocessing
import os
import shutil
from datetime import datetime
from typing import List

from PyPDF2.utils import PdfReadError
from joblib import Parallel, delayed
from signer_pdf.signer import sign

from helpers import json_serial
from models import FileProcessResult


class Processor(object):
    def __init__(self, folder_path: str, backup_folder_path: str, author: str, hash_code: str, position: str) -> None:
        self._folder_path = folder_path
        self.__backup_folder_path = backup_folder_path
        self.__author = author
        self.__hash_code = hash_code
        self.__position = position
        self.process_start_time = datetime.now()
        self.meta = {}
        os.makedirs(self.__backup_folder_path, exist_ok=True)

    _folder_path: str
    __backup_folder_path: str
    __author: str
    __hash_code: str
    __position: str

    def file_process(self, filename: str) -> FileProcessResult:
        print(filename)
        file = os.path.join(self._folder_path, filename)
        with open(file, "r+b") as inFile:
            before_size = os.path.getsize(file)
            if before_size < 1:
                print(f"{filename} - size 0")
                return FileProcessResult(filename)
            try:
                date_time = datetime.fromtimestamp(os.path.getmtime(file))
                inFile.seek(0)
                file_data = sign(
                    self.__author,
                    date_time,
                    self.__hash_code,
                    self.__position,
                    inFile
                ).getvalue()
                inFile.seek(0)
                inFile.write(
                    file_data
                )
                inFile.seek(0)
                after_size = os.path.getsize(file)
                print(f"file {filename} bytes before: {before_size} bytes after: {after_size}")
                return FileProcessResult(filename, self.__author, date_time, self.__hash_code)
            except PdfReadError:
                print(f"{filename} - PdfReadError")
                return FileProcessResult(filename)

    @staticmethod
    def __dump_results_to_json_file(results, process_start_time):
        with open(f"results-{process_start_time.strftime('%Y-%m-%d_%H-%M-%S')}.json",
                  "w", encoding="utf-16") as results_file:
            json.dump(results, results_file, indent=4, default=json_serial, ensure_ascii=False)

    def __find_files_to_process(self, last_datetime: datetime) -> List[str]:
        return list(filter(lambda x:
                           (not x.endswith('_signed.pdf')) and
                           (x.endswith('.pdf') or x.endswith('.PDF')) and
                           (os.path.getmtime(os.path.join(self._folder_path, x)) > last_datetime.timestamp()),
                           os.listdir(self._folder_path)))

    def _backup_files(self, filename: str):
        backup_folder = os.path.join(self.__backup_folder_path,
                                     f"{self.process_start_time.strftime('%Y-%m-%d_%H-%M-%S')}")
        os.makedirs(backup_folder, exist_ok=True)
        shutil.copy(os.path.join(self._folder_path, filename), os.path.join(backup_folder, filename))
        print(f"backup {filename}")

    def start_process(self):
        with open(f"meta.json", "r", encoding="utf-8") as meta_file:
            self.meta = json.load(meta_file)

        files_to_process = \
            self.__find_files_to_process(last_datetime=datetime.fromisoformat(self.meta["last_datetime"]))
        for filename in files_to_process:
            logging.info(f'selected to process {filename}')
        logging.info(f"selected {len(files_to_process)} files")

        cores_count = multiprocessing.cpu_count()

        os.makedirs(self.__backup_folder_path, exist_ok=True)

        Parallel(n_jobs=cores_count)(delayed(self._backup_files)(filename) for filename in files_to_process)

        results = Parallel(n_jobs=cores_count)(delayed(self.file_process)(filename) for filename in files_to_process)

        self.__dump_results_to_json_file(results, self.process_start_time)

        with open(f"meta.json", "w", encoding="utf-8") as meta_file:
            self.meta["last_datetime"] = datetime.now()
            json.dump(self.meta, meta_file, indent=4, default=json_serial, ensure_ascii=False)
