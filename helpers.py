from datetime import datetime

from models import FileProcessResult


def json_serial(obj):
    if isinstance(obj, datetime):
        return obj.isoformat()
    elif isinstance(obj, FileProcessResult):
        return obj.__dict__
    raise TypeError("Type %s not serializable" % type(obj))
